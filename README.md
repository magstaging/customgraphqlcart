mutation {
  createCartWithSkus(
    input: {
      cart_id: ""
      cart_items: [
        {
          data: {
            quantity: 1
            sku: "SKUBAT5"
          }
        },
        {
          data: {
            quantity: 2
            sku: "SKUBAT2"
          }
        }
      ]
    }
  ) {
    cart_id
    cart {
      items {
        id
        product {
          name
          sku
        }
        quantity
      }
    }
  }
}

if problem with loading the graphql, flush cache, redis and browser seems to help
